#ifndef __FOLIB_H__
#define __FOLIB_H__

#if _MSC_VER >= 1400
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NON_CONFORMING_SWPRINTFS
#endif

#include <assert.h>
#include <stdio.h>
#include <windows.h>

#define FO_FILEID				0x000306DF		// FO文件包标识  =>FO
#define FO_VERSION				0x00000001		// FO文件包版本  =>01.00.00
#define HASHTBL_LEN				10000

//-----------------------------------------------------------------------------
// 数据结构
#if (defined(WIN32) || defined(WIN64))
#include <pshpack1.h>
#else
#pragma pack(push,1)
#endif

// 文件包头数据结构
struct FOHeader
{
	DWORD dwMark;				// FO的数据标志0x306DF
	DWORD dwNull1;
	DWORD dwNull2;
	DWORD dwVer;				// 文件包格式的版本号
	DWORD dwBlockTableSize;		// HashTable以及BlockTable表项的多少
	DWORD dwNull3;
};

// 文件包的HashTable表的表项结构（文件名hash）
struct FOHASH
{
	DWORD dwName1;
	DWORD dwName2;
};

// 文件包的BlockTable表的表项结构（描述具体文件数据的信息）
struct FOBlock
{
	DWORD dwNull1[2];
	char szFileName[64];
	DWORD dwNull2[8];
	DWORD dwNull3;
	DWORD dwFilePos;
	DWORD dwCSize;//压缩大小
	DWORD dwFSize;//原始大小
	DWORD dwFlags;//文件标记,0=?,9=?
	DWORD dwCrc32;//?
};

struct FOFileHeader
{
	FOHeader hdr;
	FOHASH hashtbl[HASHTBL_LEN];
	FOBlock filetbl[HASHTBL_LEN];
};

#if (defined(WIN32) || defined(WIN64))
#include <poppack.h>
#else
#pragma pack(pop)
#endif

// ----------------------------------------------------------------------------
// 压缩或解压缩数据
int WINAPI Compress_zlib   (char* pbOutBuffer, int* pdwOutLength, char* pbInBuffer, int dwInLength);
int WINAPI Decompress_zlib (char* pbOutBuffer, int* pdwOutLength, char* pbInBuffer, int dwInLength);

#endif  // __FOLIB_H__
