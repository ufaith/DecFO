#include "stdafx.h"
#include "FOFile.h"
#include "RWFile.h"
#include "zlib.h"

// ============================================================================
// 压缩或解压方法
// ============================================================================
//-----------------------------------------------------------------------------
// 压缩数据方法
int WINAPI Compress_zlib(char * pbOutBuffer, int * pdwOutLength, char * pbInBuffer, int dwInLength)
{
	z_stream z; 
	int nResult;
	// 填充zlib的stream结构数据
	z.next_in   = (Bytef *)pbInBuffer;
	z.avail_in  = (uInt)dwInLength;
	z.total_in  = dwInLength;
	z.next_out  = (Bytef *)pbOutBuffer;
	z.avail_out = *pdwOutLength;
	z.total_out = 0;
	z.zalloc    = NULL;
	z.zfree     = NULL;
	// 初始化压缩数据
	*pdwOutLength = 0;
	if((nResult = deflateInit(&z, Z_DEFAULT_COMPRESSION)) == 0)
	{
		// 执行zlib的压缩
		nResult = deflate(&z, Z_FINISH);        
		if(nResult == Z_OK || nResult == Z_STREAM_END)
			*pdwOutLength = z.total_out;
		deflateEnd(&z);
	}
	return nResult;
}

//-----------------------------------------------------------------------------
// 解压缩方法
int WINAPI Decompress_zlib(char * pbOutBuffer, int * pdwOutLength, char * pbInBuffer, int dwInLength)
{
	z_stream z;
	int nResult;

	// 填充zlib的stream结构数据
	z.next_in   = (Bytef *)pbInBuffer;
	z.avail_in  = (uInt)dwInLength;
	z.total_in  = dwInLength;
	z.next_out  = (Bytef *)pbOutBuffer;
	z.avail_out = *pdwOutLength;
	z.total_out = 0;
	z.zalloc    = NULL;
	z.zfree     = NULL;
	// 初始化压缩数据
	if((nResult = inflateInit(&z)) == 0)
	{
		// 执行zlib的解压
		nResult = inflate(&z, Z_FINISH);
		*pdwOutLength = z.total_out;
		inflateEnd(&z);
	}
	return nResult;
}