#include "stdafx.h"
#include "RWFile.h"
#include <dbghelp.h>
#include <shlwapi.h>
#pragma comment(lib, "dbghelp")
#pragma comment(lib, "shlwapi")
#include "FOFile.h"

#include <vector>
#include <string>
#include <algorithm>
using namespace std;

string PathFix(const string& _RawPath)
{
	string _Path = _RawPath;
	char* p = (char*)_Path.c_str();
	while (*p)
	{
		if (*p == '/')
		{
			*p = '\\';
		}
		p++;
	}
	return _Path;
}

void DecFO(string path)
{
	string ext = RWFile::GetFileExt(path);
	if (_stricmp(ext.c_str(), ".fo"))
	{
		printf("file must .fo!!!");
		return;
	}
	string dir = RWFile::StripFileExt(path);
	RWFile::MkDir(dir);

	FILE* _pBinFile = fopen(path.c_str(), "rb");
	if (!_pBinFile) {
		printf("file .fo open error!!!");
		return;
	}
	DWORD dwHeadSize = sizeof(FOFileHeader);
	string dataHeader;
	dataHeader.resize(dwHeadSize);
	FOFileHeader& Header = *((FOFileHeader*)dataHeader.c_str());
	//--读取包文件头--
	if (fread((void*)&Header, sizeof(Header), 1, _pBinFile) != 1) {
		fclose(_pBinFile);
		printf("file .fo read header error!!!");
		return;
	}
	if (Header.hdr.dwMark != FO_FILEID)
	{
		fclose(_pBinFile);
		printf("file mark not .fo error!!!");
		return;
	}
	FOHASH* hashtbl = &Header.hashtbl[0];
	FOBlock* filetbl = &Header.filetbl[0];
	string _Buff;
	string _RawBuff;
	for (int i = 0; i < HASHTBL_LEN; i++)
	{
		FOHASH& kHash = hashtbl[i];
		if (kHash.dwName1 == 0 || kHash.dwName2 == 0)
		{
			continue;
		}
		FOBlock& kFileBlock = filetbl[i];
		if (kFileBlock.dwFSize == 0 || kFileBlock.dwCSize == 0)
		{
			continue;
		}
		if (strlen(kFileBlock.szFileName) == 0)
		{
			continue;
		}
		fseek(_pBinFile, kFileBlock.dwFilePos + dwHeadSize, SEEK_SET);
		if (kFileBlock.dwFSize != kFileBlock.dwCSize)
		{
			_RawBuff.resize(kFileBlock.dwFSize);
			_Buff.resize(kFileBlock.dwCSize);
			if (fread((void*)_Buff.c_str(), kFileBlock.dwCSize, 1, _pBinFile) != 1) {
				fclose(_pBinFile);
				return;
			}
			int outputBufferSize = (int)kFileBlock.dwFSize;
			int inputBufferSize = (int)kFileBlock.dwCSize;
			int ret = Decompress_zlib((char*)_RawBuff.c_str(), &outputBufferSize, (char*)_Buff.c_str(), (int)inputBufferSize);
			_RawBuff.resize(outputBufferSize);
		}
		else
		{
			_RawBuff.resize(kFileBlock.dwFSize);
			if (fread((void*)_RawBuff.c_str(), kFileBlock.dwFSize, 1, _pBinFile) != 1) {
				fclose(_pBinFile);
				return;
			}
		}
		path = dir + "\\" + PathFix(kFileBlock.szFileName);
		if (RWFile::GetFileExt(path) == ".ab")
		{
			path = RWFile::StripFileExt(path) + ".pkm";
			_RawBuff = _RawBuff.substr(16);
		}
		RWFile::MkDir(path, true);
		RWFile::SaveFile(path, _RawBuff);
	}
	fclose(_pBinFile);
	printf("fo unpack OK!!!");
}

void EncFO(string path)
{
	//if (path[path.length()-1] == '\\')
	//{
	//	path = path.substr(0, path.length()-1);
	//}
	//string tpkName = path + ".fo";
	//BOOL bRet = FOPackFileAll(tpkName.c_str(), NULL,  path.c_str(), NULL);
	//if (!bRet)
	//{
	//	printf("fo pack error!!!");
	//	return;
	//}
	//printf("fo pack OK!!!");
}

int main(int argc, char **argv)
{
	printf("DecFO.exe v1.0 by zhupf QQ:335264417\n");
	printf("DecFO.exe for 征途手游!!!\n");
	if (argc == 2)
	{
		string path = argv[1];
		if (!PathFileExists(path.c_str()))
		{
			printf("file not found!!!");
			return -1;
		}
		if (PathIsDirectory(path.c_str()))
		{
			EncFO(path);
		}
		else
		{
			DecFO(path);
		}
	}
    return 0;
}