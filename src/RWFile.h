#pragma once
#include <string>
#include <vector>
#include <fstream>
using std::string;
using std::vector;
using std::ifstream;

class RWFile
{
public:
	RWFile(void);
	~RWFile(void);
	static int GetFileSize(const string& _FileName);
	static bool LoadFile(const string& _FileName,string& _Buff);
	static bool SaveFile(const string& _FileName,const string& _Buff);
	static bool LoadAllLine(const string& _FileName,vector<string>& _Lines);
	static bool AppendFile(const string& _FileName,string& _Buff);
	static string GetExtFullPath();
	static string GetExeFullDir();
	static string StripFileExt(string _FileName);
	static string GetFileExt(string _FileName);
	static string GetFileNameFromPath(const string& _Path);
	static void MkDir(const string& _FileOrDir, bool _IsFile = false);
	static void FindAllFile(const string& _RootName, const string& _DirName, const string& _Mark, vector<string>& _FileList);
};
