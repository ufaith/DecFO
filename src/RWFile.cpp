#include "StdAfx.h"
#include "RWFile.h"
#include <dbghelp.h>
#pragma comment(lib, "dbghelp")

RWFile::RWFile(void)
{
}

RWFile::~RWFile(void)
{
}

int RWFile::GetFileSize(const string& _FileName)
{
	int _FileSize = 0;
	FILE* _pBinFile = fopen(_FileName.c_str(), "rb");
	if(_pBinFile){
		fseek(_pBinFile, 0, SEEK_END);
		_FileSize = ftell(_pBinFile);
		fclose(_pBinFile);
	}
	return _FileSize;
}

bool RWFile::LoadFile(const string& _FileName,string& _Buff)
{
	bool _ret = false;
	FILE* _pBinFile = fopen(_FileName.c_str(), "rb");
	if(_pBinFile){
		fseek(_pBinFile, 0, SEEK_END);
		UINT _FileSize = ftell(_pBinFile);
		fseek(_pBinFile, 0, SEEK_SET);
		_Buff.resize(_FileSize);
		_ret = true;
		if(fread((void*)_Buff.c_str(), _FileSize, 1, _pBinFile) != 1) {
			_Buff = "";
			_ret = false;
		}
		fclose(_pBinFile);
	}
	return _ret;
}

bool RWFile::SaveFile(const string& _FileName,const string& _Buff)
{
	bool _ret = false;
	FILE* _pBinFile = fopen(_FileName.c_str(), "wb");
	if(_pBinFile){
		if(fwrite((void*)_Buff.c_str(), _Buff.size(), 1, _pBinFile) == 1) {
			_ret = true;
		}
		fclose(_pBinFile);
	}
	return _ret;
}

bool RWFile::AppendFile(const string& _FileName,string& _Buff)
{
	bool _ret = false;
	FILE* _pBinFile = fopen(_FileName.c_str(), "ab");
	if(_pBinFile){
		if(fwrite((void*)_Buff.c_str(), _Buff.size(), 1, _pBinFile) == 1) {
			_ret = true;
		}
		fclose(_pBinFile);
	}
	return _ret;
}

bool RWFile::LoadAllLine(const string& _FileName,vector<string>& _Lines)
{
	_Lines.clear();
	ifstream fin(_FileName);
	if (!fin.is_open())
	{
		return false;
	}
	string s;
	while(getline(fin,s))
	{
		if (s != "")
		{
			_Lines.push_back(s);
		}
	}
	return true;
}

string RWFile::GetExtFullPath()
{
	string exe_path(256, NULL);
	GetModuleFileName(NULL, (PSTR)exe_path.c_str(), exe_path.length());
	exe_path = exe_path.c_str();
	return exe_path;
}

string RWFile::GetExeFullDir()
{
	string exe_path = GetExtFullPath();
	string exe_dir = exe_path.substr(0, exe_path.find_last_of('\\') + 1);
	return exe_dir;
}

string RWFile::StripFileExt(string _FileName)
{
	int _Pos = -1;
	if ((_Pos = _FileName.rfind('.')) != string::npos) {
		_FileName = _FileName.substr(0, _Pos);
	}
	return _FileName;
}

string RWFile::GetFileExt(string _FileName)
{
	int _Pos = -1;
	if ((_Pos = _FileName.rfind('.')) != string::npos) {
		_FileName = _FileName.substr(_Pos);
	}
	return _FileName;
}

string RWFile::GetFileNameFromPath(const string& _Path)
{
	int _Pos = -1;
	if ((_Pos = _Path.rfind('\\')) != string::npos) {
		string _FileName = _Path.substr(_Pos + 1, _Path.size() - _Pos);
		return _FileName;
	}
	return "";
}

void RWFile::MkDir(const string& _FileOrDir, bool _IsFile/* = false*/)
{
	string _Path = _FileOrDir;
	if (!_IsFile && !_Path.empty() && _Path[_Path.size() - 1] != '\\') {
		_Path += "\\";
	}
	::MakeSureDirectoryPathExists(_Path.c_str());
}

void RWFile::FindAllFile(const string& _RootName, const string& _DirName, const string& _Mark, vector<string>& _FileList)
{
	WIN32_FIND_DATAA _FindData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	string _FindPath = _DirName.empty() ? (_RootName + "\\*.*") : (_RootName + "\\" + _DirName + "\\*.*");
	hFind = FindFirstFileA(_FindPath.c_str(), &_FindData);
	if (hFind == INVALID_HANDLE_VALUE)
		return;

	string _FindFileName;
	do {
		_FindFileName = _FindData.cFileName;
		if (_FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (_FindFileName == "." || _FindFileName == "..") {
				continue;
			}
			else {
				string _NewDir = _DirName.empty() ? _FindFileName : _DirName + "\\" + _FindFileName;
				FindAllFile(_RootName, _NewDir, _Mark, _FileList);
			}
		}
		else {
			if (_Mark == "*.*" || GetFileExt(_FindFileName) == _Mark)
			{
				string _File = _DirName.empty() ? _FindFileName : _DirName + "\\" + _FindFileName;
				_FileList.push_back(_File);
			}
		}
	} while (FindNextFileA(hFind, &_FindData) != 0);
	FindClose(hFind);
}